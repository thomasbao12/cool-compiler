/*
 *  The scanner definition for COOL.
 */

/*
 *  Stuff enclosed in %{ %} in the first section is copied verbatim to the
 *  output, so headers and global definitions are placed here to be visible
 * to the code in the file.  Don't remove anything that was here initially
 */
%{
#include <cool-parse.h>
#include <stringtab.h>
#include <utilities.h>

/* The compiler assumes these identifiers. */
#define yylval cool_yylval
#define yylex  cool_yylex

/* Max size of string constants */
#define MAX_STR_CONST 1025
#define YY_NO_UNPUT   /* keep g++ happy */

extern FILE *fin; /* we read from this file */

/* define YY_INPUT so we read from the FILE fin:
 * This change makes it possible to use this scanner in
 * the Cool compiler.
 */
#undef YY_INPUT
#define YY_INPUT(buf,result,max_size) \
	if ( (result = fread( (char*)buf, sizeof(char), max_size, fin)) < 0) \
		YY_FATAL_ERROR( "read() in flex scanner failed");

char string_buf[MAX_STR_CONST]; /* to assemble string constants */
char *string_buf_ptr;

extern int curr_lineno;
extern int verbose_flag;

extern YYSTYPE cool_yylval;

/*
 *  Add Your own definitions here
 */

#define APPEND(c) \
	if (string_buf_ptr - string_buf + 1 >= MAX_STR_CONST) { \
		BEGIN(STRING_ERROR); \
		cool_yylval.error_msg = "String constant too long"; \
		return ERROR; \
	} \
	*string_buf_ptr++ = c;

int open_comments = 0;

%}

%Start COMMENT
%Start STRING
%Start STRING_ERROR
/*
 * Define names for regular expressions here.
 */


/*
 * Multiple character operators
 */

DARROW          	=>

INT_CONST	[0-9]+
BOOL_CONST	t[rR][uU][eE]|f[aA][lL][sS][eE]
TYPEID		[A-Z][a-zA-Z0-9_]*
OBJECTID	[a-z][a-zA-Z0-9_]*

%%

 /*
  *  Comments
  */

<INITIAL,COMMENT>{ 
	"(*"		{ 
				++open_comments; 
				BEGIN(COMMENT); 
				//printf("%d\n", open_comments);
			}
}
<INITIAL>{
	"--".*		;
}
<COMMENT>{

	"*)"		{ 
				--open_comments;
				if (open_comments == 0) 
					BEGIN(INITIAL);
				//printf("%d\n", open_comments);
			}
	\n		++curr_lineno;
	.		;
	<<EOF>>		{
				BEGIN(INITIAL);
				cool_yylval.error_msg = "EOF in comment";
				return ERROR;
			}

}
"*)"			{ 
				cool_yylval.error_msg = "Unmatched *)"; 
				return ERROR; 
			}

 /*
  * Strings
  */

<INITIAL>{
	\"		{	
				string_buf_ptr = string_buf; 
				BEGIN(STRING);
				//printf("string\n");
			}
}
<STRING>{
	\"		{
				//printf("stringEnd\n");
				*string_buf_ptr = '\0';
				BEGIN(INITIAL);
				cool_yylval.symbol = stringtable.add_string(string_buf);
				return STR_CONST;
			}
	[^\\\n\0\"]+	{
				if (string_buf_ptr - string_buf + yyleng + 1 >= MAX_STR_CONST) {
					BEGIN(STRING_ERROR);
					cool_yylval.error_msg = "String constant too long";
					return ERROR;
				}
				strcpy(string_buf_ptr, yytext);
				string_buf_ptr += yyleng;
			}
	<<EOF>>		{
				BEGIN(INITIAL);
				cool_yylval.error_msg = "EOF in string constant";
				return ERROR;
			}
	\\b	APPEND('\b');
	\\f	APPEND('\f');
	\\t	APPEND('\t');
	\n		{
				++curr_lineno;
				BEGIN(INITIAL);
				cool_yylval.error_msg = "Unterminated string constant";
				return ERROR;
			}
	\\n		APPEND('\n');
	\\\n		{
				++curr_lineno;
				APPEND('\n');
			}
	\0		{
				BEGIN(STRING_ERROR);
				cool_yylval.error_msg = "String contains null character.";
				return ERROR;
			}
	\\0		APPEND('0');
	\\\0		{
				BEGIN(STRING_ERROR);
				cool_yylval.error_msg = "String contains escaped null character.";
				return ERROR;
			}
	\\.		APPEND(yytext[1]);
}
<STRING_ERROR>{
	\"	BEGIN(INITIAL);
	\n	{
			++curr_lineno;
			BEGIN(INITIAL);
		}
	\\\n	++curr_lineno;
	.	;
}

<INITIAL>{
 /*
  * operators. 
  */

";"		return ';';
"{"		return '{';
"}"		return '}';
"("		return '(';
","		return ',';
")"		return ')';
":"		return ':';
"@"		return '@';
"."		return '.';
"+"		return '+';
"-"		return '-';
"*"		return '*';
"/"		return '/';
"~"		return '~';
"<"		return '<';
"="		return '=';
"<-"		return ASSIGN;
"=>"		return DARROW;
"<="		return LE;

 /*
  *	Keywords
  */

[cC][lL][aA][sS][sS]               	return CLASS;
[eE][lL][sS][eE]                    	return ELSE;
f[aA][lL][sS][eE]                   	{
                                        	cool_yylval.boolean = false;
                                        	return BOOL_CONST;
                                    	}
[fF][iI]                            	return FI;
[iI][fF]                            	return IF;
[iI][nN]                            	return IN;
[iI][nN][hH][eE][rR][iI][tT][sS]    	return INHERITS;
[iI][sS][vV][oO][iI][dD]            	return ISVOID;
[lL][eE][tT]                        	return LET;
[lL][oO][oO][pP]                    	return LOOP;
[pP][oO][oO][lL]                    	return POOL;
[tT][hH][eE][nN]                    	return THEN;
[wW][hH][iI][lL][eE]                	return WHILE;
[cC][aA][sS][eE]                    	return CASE;
[eE][sS][aA][cC]                    	return ESAC;
[nN][eE][wW]                        	return NEW;
[oO][fF]                            	return OF;
[nN][oO][tT]                        	return NOT;
t[rR][uU][eE]                       	{
                                        	cool_yylval.boolean = true;
                                        	return BOOL_CONST;
                                    	}



 /*
  *  The multiple-character operators.
  */

{INT_CONST} { 
	cool_yylval.symbol = inttable.add_string(yytext);
	return (INT_CONST); 
}
{TYPEID} {
	cool_yylval.symbol = idtable.add_string(yytext);
	return TYPEID;
}
{OBJECTID} {
	cool_yylval.symbol = idtable.add_string(yytext);
	return OBJECTID;
}
}

 /*
  * Keywords are case-insensitive except for the values true and false,
  * which must begin with a lower-case letter.
  */


 /*
  *  String constants (C syntax)
  *  Escape sequence \c is accepted for all characters c. Except for 
  *  \n \t \b \f, the result is c.
  *
  */

 /*
  *  Whitespace
  */

[\t\v\f\r ]+		;
\n 			++curr_lineno; 
.			{
				cool_yylval.error_msg = yytext;
				return ERROR;
			}

%%
