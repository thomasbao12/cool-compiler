(*
 *  CS164 Fall 94
 *
 *  Programming Assignment 1
 *    Implementation of a simple stack machine.
 *
 *  Skeleton file
 *)

class List {
   -- Define operations on empty lists.

   isNil() : Bool { true };

   -- Since abort() has return type Object and head() has return type
   -- String, we need to have an String as the result of the method body,
   -- even though abort() never returns.

   head()  : String { { abort(); ""; } };

   -- As for head(), the self is just to make sure the return type of
   -- tail() is correct.

   tail()  : List { { abort(); self; } };

   -- When we cons and element onto the empty list we get a non-empty
   -- list. The (new Cons) expression creates a new list cell of class
   -- Cons, which is initialized by a dispatch to init().
   -- The result of init() is an element of class Cons, but it
   -- conforms to the return type List, because Cons is a subclass of
   -- List.

   cons(s : String) : List {
      (new Cons).init(s, self)
   };

};


(*
 *  Cons inherits all operations from List. We can reuse only the cons
 *  method though, because adding an element to the front of an emtpy
 *  list is the same as adding it to the front of a non empty
 *  list. All other methods have to be redefined, since the behaviour
 *  for them is different from the empty list.
 *
 *  Cons needs two attributes to hold the integer of this list
 *  cell and to hold the rest of the list.
 *
 *  The init() method is used by the cons() method to initialize the
 *  cell.
 *)

class Cons inherits List {

   car : String;-- The element in this list cell

   cdr : List;	-- The rest of the list

   isNil() : Bool { false };

   head()  : String { car };

   tail()  : List { cdr };

   init(s : String, rest : List) : List {
      {
	 car <- s;
	 cdr <- rest;
	 self;
      }
   };

};

class Main inherits IO {


   mylist : List;
   i : String;
   s1 : String;
   s2 : String;
   n1 : Int;
   n2 : Int;

   -- Print all elements of the list. Calls itself recursively with
   -- the tail of the list, until the end of the list is reached.

   print_list(l : List) : Object {
      if l.isNil() then out_string("")
                   else {
			   out_string(l.head());
			   out_string("\n");
			   print_list(l.tail());
		        }
      fi
   };


   main() : Object {
			{
				mylist <- new List;
				while (not (i = "x")) loop
					{
						i <- in_string();
						if (i = "d") then print_list(mylist)
												 else {
						if (i = "e") then {
							if mylist.isNil() then mylist <- mylist else
							let top : String <- mylist.head() in
								if (top = "+") then {
									mylist <- mylist.tail();
									s1 <- mylist.head();
									mylist <- mylist.tail();
									s2 <- mylist.head();
									n1 <- (new A2I).a2i(s1); 
									n2 <- (new A2I).a2i(s2); 
									mylist <- mylist.tail().cons((new A2I).i2a(n1 + n2));
								}
															 else 
								if (top = "s") then {
									mylist <- mylist.tail();
									s1 <- mylist.head();
									mylist <- mylist.tail();
									s2 <- mylist.head();
									mylist <- mylist.tail().cons(s1).cons(s2);
								}
															 else mylist <- mylist
								fi
								fi
								fi;
						}
												 else mylist <- mylist.cons(i)
						fi;
						}
						fi;
					}
				pool;
			}
		};

};
